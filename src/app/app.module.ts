// app.module.ts

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HeaderComponent } from './header/header.component';
// import { HeaderComponent } from './header/header.component';
// import { ShowcustomersComponent } from './showcustomers/showcustomers.component';
// import { EventsComponent } from './events/events.component';
// import { LogoutComponent } from './logout/logout.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HeaderComponent,
    // HeaderComponent,
    // ShowcustomersComponent,
    // EventsComponent,
    // LogoutComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot([
      { path: 'login', component: LoginComponent },
      { path: 'register', component: RegisterComponent },
      // Add more routes as needed
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }