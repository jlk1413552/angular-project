import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent implements OnInit {

  emailId: any;
  password: any;
  customers: any;

 //Dependency Injection for Router
 constructor(private router: Router) {

  //Adding Employees for Login Authentication
  this.customers = [
    {username: 'karunya',emailId:'karunya@gmail.com',password:'1234',confirmpassword:'1234',doe:'2-01-2024',address:'hyd',venue:'Goa'}
  ];
  }

  ngOnInit() {
  }

  loginSubmit(loginForm: any) {
    //alert(loginForm);
    //alert("EmailId:" + loginForm.value.emailId + "\nPassword:" + loginForm.value.password);

    console.log(loginForm);

    if(loginForm.emailId === "jasmitha@gmail.com" && loginForm.password === "12345") {
      alert("Login Success!!!");
    } else {
      alert("Invalid Credentials");
    }


  }

  submit() {
    alert("EmailId: " + this.emailId + "\nPassword: " + this.password);
    console.log(this.emailId);
    console.log(this.password);
  }

}